require 'date'
require 'rubygems'
require 'tweetstream'

require "#{Rails.root}/app/helpers/application_helper"
include ApplicationHelper

# get the set of keywords to be tracked
def keywords_to_track
  keywords = Set.new
  Tracking.each do |t|
    t.clean_keywords.each { |k| keywords << k }
  end

  keywords
end

desc 'Run a listener for the current keywords'
task :track => :environment do |t, args|
  # create a tweetstream client
  TweetStream.configure do |config|
    config.consumer_key       = TRACKER_CONFIG['consumer_key']
    config.consumer_secret    = TRACKER_CONFIG['consumer_secret']
    config.oauth_token        = TRACKER_CONFIG['oauth_token']
    config.oauth_token_secret = TRACKER_CONFIG['oauth_token_secret']
    config.auth_method        = :oauth
  end

  client = TweetStream::Client.new.on_error do |message|
    log "tracker error: #{message}"
  end

  # keep collecting tweets
  while true
    keywords = keywords_to_track

    # setup a watcher thread to stop the current tracking
    # changes on the keywords so the tracker can be restarted
    watcher = Thread.new do
      sleep TRACKER_CONFIG['reload_interval']
      keywords_now = keywords_to_track
      if keywords_now != keywords
        client.stop
        break
      end
    end

    keywords_list = keywords.to_a
    log "tracking for #{keywords_list}"
    client.track(keywords_list) do |status|
      tweet = Tweet.new(:twitter_id   => status.id,
                        :twitter_user => status.user[:screen_name],
                        :timestamp    => status.created_at,
                        :text         => status.text)

      # check if to which trackings this tweet belong
      # ignoring spaces and punctuation
      tokens = status.text.split(/\s+/).map { |w| w.gsub(/[^[:alnum:]#]/, '').downcase }
      Tracking.each do |tracking|
        contains = true
        unless (tracking.clean_keywords & tokens) != tracking.clean_keywords
          tweet.trackings << tracking
   
          # notify user if it is the case
          if tracking.notify == "instantly"
            TrackingNotifier.notify_tweet(tweet, tracking)
          end
        end
      end

      # only save tweet if it matches any complete tracking
      tweet.save unless tweet.trackings.empty?
    end
  end
end
