require 'date'

require "#{Rails.root}/app/helpers/application_helper"
include ApplicationHelper

def notify_tracking(frequency)
  # get the timestamp of the last hourly notification
  last_notification = Notification.where({ :frequency => frequency }).max("timestamp")
  last_timestamp ||= DateTime.new(0)

  # create this notification's log
  notification = Notification.new :timestamp => DateTime.now.utc,
                                  :frequency => frequency

  # notify each tracking owner by mail
  Tracking.where({ :notify => frequency }).each do |track|
    TrackingNotifier.notify_since(last_timestamp, track)
  end

  notification.save
end

namespace :notify do
  desc 'Sends the hourly notifications'
  task :hourly => :environment do
    log "hourly mail notification"
    notify_tracking(:hourly)
  end 

  desc 'Sends the daily notifications'
  task :daily => :environment do
    log "daily mail notification"
    notify_tracking(:daily)
  end
end
