# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

puts 'cleaning models'
models = [User, Tracking, Tweet, Notification]
models.each do |m|
  puts "  #{m}"
  m.destroy_all
end

puts 'setting up root user'
pass = 'bimboninho'
user = User.create! :name => 'root', :email => 'root@brunoro.org', 
                    :password => pass, :password_confirmation => pass

puts 'adding some trackings to this user'
keywords = ['tame impala', 'cruzeiro']
keywords.each do |k|
  puts "  #{k}"
  t = Tracking.create :keywords => k, :user => user, :notify => :hourly
end
