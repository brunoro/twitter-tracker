    
    |         o|    |              |                   |              
    |--- . . ..|--- |--- ,---.,---.|--- ,---.,---.,---.|__/ ,---.,---.
    |    | | |||    |    |---'|    |    |    ,---||    |  \ |---'|    
    `---'`-'-'``---'`---'`---'`    `---'`    `---^`---'`   ``---

About
-----

A simple Twitter alert service with e-mail notifications.
Sign up, register a set of keywords and receive notifications
of tweets matching these keywords.

### Features

* Multiple keyword tracking, eg.: `justin bieber`.
* Selecting notification frequency between daily, hourly, instantly or never.
* Exporting tracked tweets on a CSV file.

### Try it
At [http://purple.endofinternet.org:8000](http://purple.endofinternet.org:8000)


### Author
Gustavo Brunoro (root at brunoro dot org)


Technical details
-----------------

### Concepts
There are three entities (or models), in the system:

* _Tweet_: a message collected from Twitter. 
* _Tracking_: a set of keywords separed by space; 
              a _Tracking_ contains many _Tweets_;
	      the same _Tweet_ can belong to two different _Trackings_;
	      a _Tweet_ must match all keywords of a _Tracking_ to be contained on it;
	      the keywords on _Tracking_ cannot be modified.
* _User_: has many _Trackings_;
          one _Tracking_ can only belong to one user;


### Modules
* _Tracker_: service that collects _Tweets_ from the Twitter Streaming API
             according to the keywords present on all _Trackings_ on the 
	     database. As of network capacity, only one connection is kept
	     open with the Twitter API, and this connection collects the set
	     of all keywords needed.
	     A separate thread checks for changes on the set of  keywords to track 
	     on the database on a fixed frequency (default 300 seconds). 
	     If there are any new keywords, the connection with the API is restarted 
	     for the new set.
	     When a tweet is recieved, all _Trackings_  are checked for a match. 
	     English stopwords and punctuation are not considered on keywords. 
	     The matching case insensitive. A _Notifier_ may also be called if 
	     the matched _Tracking_ notification option is set to _instantly_.
* _Notifier_: module that notifies matched _Tweets_ to _Users_. It has two methods:
              one to notify one _Tweet_ when it arrives (notification option set to
	      _instantly_) or to notify a set of _Tweets_ on reports (notification set
	      to _daily_ or _hourly_). On the case of the instant notification, the
	      method is called from within the _Tracker_ module. For the periodical
	      notifications, the call is made from a task registered to run at the 
	      given frequency on the server to notify all _Trackings_ defined at that
	      option.
* _Web Interface_: module that allows _Users_ to sign in, create and remove _Trackings_, edit
                   their notification options and view its current _Tweets_ and _Trackings_.
		   The interface shows all _Tweet_ timestamps on the user-defined timezone.
		   There is an option to export all _Tweets_ from a _Tracking_ to a CSV file.

### Technologies
* [Ruby on Rails](http://rubyonrails.org): robust web framework for the Ruby language. Used to build the user interface and
                 for sending mails through a SMTP server.
* [MongoDB](http://mongodb.org): scalable NoSQL database based on a collection/document abstraction.
* [Bootstrap](twitter.github.com/bootstrap/): CSS/Javascript toolkit for quickly making applications have a pleasant look-and-feel
* [Twitter Streaming API](https://dev.twitter.com/docs/streaming-apis): service from Twitter that allows collecting tweets filtered by keywords streamed through an open connection.
* Cron: UNIX daemon to schedule jobs. Used here for scheduling the frequent _Notifiers_.

### Deploy
The application is currently deployed using [Capistrano](https://github.com/capistrano/capistrano) over the [Unicorn](http://unicorn.bogomips.org/) server. 
The corresponding configuration files are found in `config/deploy.rb` and `config/unicorn.rb`.
The tracker has to be started manually using the provided rake task `rake track`.
The tracker configuration (including Twitter API keys) have to be setup on `config/tacker.yml`.
Cronjobs are setup manually using the tasks `rake notify:hourly` e `rake notify:daily`.

### Directory structure
The system is disposed by the standard file structure for Rails projects. Some relevant files:

* `Gemfile`: lists the required gems for the project, which are installed by running `bundle install`
* `app/controllers`: directory containing the controller classes.
* `app/helpers/application_helper.rb`: defines the helper methods used throughout the application, such as date formatting.
* `app/mailers/tracking_notifier.rb`: class that provides methods for frequent and instant notifications.
* `app/models`: definition of the entities manipulated on the system.
* `app/views`: all the code used to generate the html pages served on the webapp, the csv format used for exporting trackings and the mail html notifications.
* `config/application.rb`: main configuration file for the project.
* `config/deploy.rb`: definition of the tasks for deployment using Capistrano.
* `config/unicorn.rb`: configuration of the Unicorn application server.
* `config/routes.rb`: maps all the URLs served by the app.
* `config/tracker.yml`: contains the Twitter API keys and parameters of the tracker.
* `config/mongoid.yml`: parameters for connecting with MongoDB.
* `config/mailer.rb`: ActionMailer SMTP settings.
* `db/seeds.rb`: task that puts the database on the inital state.
* `lib/tasks/notify.rake`: rake tasks for hourly and daily notification.
* `lib/tasks/track.rake`: implementation of the Twitter tracker.
