source 'https://rubygems.org'

gem 'rails', '3.2.6'

# MongoDB for schemaless storage
gem 'mongoid'
gem 'bson_ext'

# Authentication
gem 'devise'

# Getting data from Twitter Streaming API
gem 'tweetstream'
gem 'stopwords'

# Gems used only for assets and not required
# in production environments by default.
group :assets do
  gem 'sass-rails',   '~> 3.2.3'
  gem 'coffee-rails', '~> 3.2.1'

  # See https://github.com/sstephenson/execjs#readme for more supported runtimes
  gem 'therubyracer', :platforms => :ruby

  gem 'uglifier', '>= 1.0.3'
end

# Testing with rspec
group :test do
  gem 'rspec'
  gem 'rspec-rails'
  gem 'mongoid-rspec'
  gem 'factory_girl_rails'
  gem 'database_cleaner'
end

# User interface goodness
gem 'jquery-rails'
gem 'bootstrap-sass-rails'

# Deploy with Capistrano
gem 'capistrano'

# Use Unicorn as the app server
gem 'unicorn'
