require 'date'

module ApplicationHelper
  def format_date(date)
    date.strftime("%Y-%m-%d %H:%M")
  end

  def in_user_timezone(date, user)
    date.in_time_zone(user.timezone)
  end

  def log(str)
    puts "[#{format_date(DateTime.now)}] #{str}"
  end
end
