class TrackingNotifier < ActionMailer::Base
  default :from => "mailer@brunoro.org"

  def notify_since(timestamp, tracking)
    @tracking  = tracking
    @tweets    = tracking.tweets_since(timestamp)
    
    # avoid sending empty mails
    if @tweets.empty?
      return
    end
  
    @timestamp = timestamp
    @timestamp = @tweets.reduce do |newer, turn| 
      turn.timestamp > newer.timestamp ? turn : newer
    end.timestamp unless timestamp != DateTime.new(0)

    num = @tweets.size
    log "notifying #{num} #{num == 1 ? "tweet" : "tweets"} to <#{@tracking.user.email}> for '#{tracking.keywords}'"

    subject = "#{tracking.notify.capitalize} notification for '#{tracking.keywords}'"
    mail(:to      => tracking.user.email, 
         :subject => subject).deliver
  end
  
  def notify_tweet(tweet, tracking)
    @tracking  = tracking
    @tweet     = tweet

    log "notifying a tweet to <#{@tracking.user.email}> for '#{@tracking.keywords}'."

    subject = "Tweet notification for '#{tracking.keywords}'"
    mail(:to      => tracking.user.email, 
         :subject => subject).deliver
  end
end
