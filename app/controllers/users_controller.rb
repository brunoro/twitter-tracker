class UsersController < ApplicationController
  before_filter :authenticate_user!

  def trackings
    @trackings = current_user.trackings
  end

end
