class TrackingsController < ApplicationController
  before_filter :authenticate_user!
  
  def show
    @tracking = Tracking.find(params[:id])
    
    redirect_to "/trackings" unless @tracking
    redirect_to "/trackings" unless @tracking.user == current_user

    respond_to do |format|
      format.html
      format.csv
    end
  end

  def create
    data = params[:new]
    data[:user] = current_user

    begin
      t = Tracking.create(data)
      flash[:notice] = "Successfully registered '#{t.keywords}'. Tracking may take a few minutes to start."
    rescue
      flash[:error] = "Error creating new tracking"
    end

    redirect_to "/trackings"
  end

  def update
    begin
      t = Tracking.find(params[:id])

      # only the owner can edit it!
      if t.user == current_user
        # only notifications are editable!
        t.notify = params[:notify]
        t.save
        flash[:notice] = "Notifications for tracking '#{t.keywords}' changed to '#{t.notify}'"
      end
    rescue
      flash[:error] = "Error updating tracking"
    end

    redirect_to "/tracking/#{params[:id]}"
  end

  def delete
    begin
      t = Tracking.find(params[:id])
      keywords = t.keywords

      # only the owner can delete it!
      if t.user == current_user
        t.delete
        flash[:notice] = "Successfully stopped tracking '#{keywords}'"
      end
    rescue
      flash[:error] = "Error deleting tracking"
    end

    redirect_to "/trackings"
  end
end
