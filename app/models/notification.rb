class Notification
  include Mongoid::Document

  field :frequency,   :type => String
  field :timestamp,   :type => DateTime

end
