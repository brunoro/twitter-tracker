class Tracking
  include Mongoid::Document

  # each tracking can be notified never, instantly, hourly or daily
  field :notify,         :type => String, :default => 'instantly'
  field :keywords,       :type => String

  # downcased and tokenized keywords without punctuation and stopwords
  def clean_keywords
    all_clean = keywords.split(/\s+/).map { |w| w.gsub(/[^[:alnum:]#]/, '').downcase }
    all_clean - Stopwords::STOP_WORDS
  end

  # belongs to a user
  belongs_to :user

  # has many tweets 
  has_and_belongs_to_many :tweets
  
  # it should have an user and keywords
  validates_presence_of :user, :keywords

  # get all tweets created since the provided timestamp
  def tweets_since(timestamp)
    tweets.where({ :timestamp.gte => timestamp }).to_a
  end
end
