class Tweet
  include Mongoid::Document

  # tweet data
  field :text,         :type => String
  field :twitter_id,   :type => String
  field :twitter_user, :type => String
  field :timestamp,    :type => DateTime

  # a tweet can cite more than one tracking
  has_and_belongs_to_many :trackings
  
  # this data always should come with collected tweets
  validates_presence_of :text, :timestamp, :twitter_id, :twitter_user
  
  # there's only one tweet with this id
  validates_uniqueness_of :twitter_id

  def link
    "https://twitter.com/#{twitter_user}/statuses/#{twitter_id}"
  end
end
