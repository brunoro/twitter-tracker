TwitterTracker::Application.routes.draw do
  root :to => "home#index"
  get 'home/index'
  
  devise_for :users
  
  get  '/trackings'           => "users#trackings"
  get  '/tracking/:id'        => "trackings#show"
  get  '/tracking/:id/delete' => "trackings#delete"

  post '/tracking/:id/update' => "trackings#update"
  post '/tracking/create'     => "trackings#create"
end
