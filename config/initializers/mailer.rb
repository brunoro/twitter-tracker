ActionMailer::Base.smtp_settings = {
  :user_name            => 'your-email-username',
  :password             => 'your-email-password',
  :address              => 'your-smtp-server',
  :domain               => 'your-HELO-domain-name-if-needed',
  :port                 => 'your-smtp-server-port',
  :enable_starttls_auto => true,
  :authentication       => :plain,
}
