require "bundler/capistrano"

set :default_environment, { 
  'PATH' => "$PATH:path-to-your-gem-bin-directory-if-not-the-default"
}

default_run_options[:pty] = true
ssh_options[:port] = 2222
set :use_sudo, false

set :domain,         "your-application-domain"
set :appdir,         "your-application-folder"
set :application,    "your-application-name"
set :repository,     "your-application-repository.git"

set :scm, :git

role :web, "#{domain}"
role :app, "#{domain}"
role :db,  "#{domain}", :primary => true 

set :deploy_to, appdir
set :deploy_via, :export

namespace :deploy do
  desc "Start unicorn"
  task :start, :except => { :no_release => true } do
    run "cd #{current_path} ; bundle exec unicorn_rails -c config/unicorn.rb -D"
  end

  desc "Stop unicorn"
  task :stop, :except => { :no_release => true } do
    run "kill -s QUIT `cat /tmp/unicorn.#{application}.pid`"
  end 
end
