env = ENV["RAILS_ENV"]

# number of processes to handle requests
worker_processes 4

# listen on a UNIX socket and on a TCP port
listen "/tmp/unicorn.your-application-name.socket", :backlog => 64

# preload the app for more speed
preload_app true

timeout 30
pid "/tmp/unicorn.your-apllication-folder.pid"

# capistrano deploy opts
shared_path = "your-application-folder/shared"
stderr_path = "#{shared_path}/log/unicorn.stderr.log"
stdout_path = "#{shared_path}/log/unicorn.stdout.log"

before_fork do |server, worker|
  # kill the master process that belongs to the .oldbin PID.
  # this enables 0 downtime deploys.
  old_pid = "/tmp/unicorn.my_site.pid.oldbin"
  if File.exists?(old_pid) && server.pid != old_pid
    begin
      Process.kill("QUIT", File.read(old_pid).to_i)
    rescue Errno::ENOENT, Errno::ESRCH
      # someone else did our job for us
    end
  end
end
