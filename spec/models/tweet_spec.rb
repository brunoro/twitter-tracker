require 'spec_helper'
require 'date'

describe Tweet do
  
  it "should have some text" do
    t = Tweet.new(:twitter_id => "123456",
                  :twitter_user => "dummy",
                  :timestamp => DateTime.now)
    t.should_not be_valid
  end

  it "should come from an user" do
    t = Tweet.new(:text => "text",
                  :twitter_id => "123456",
                  :timestamp => DateTime.now)
    t.should_not be_valid
  end

  it "should have a twitter id" do
    t = Tweet.new(:text => "text",
                  :twitter_user => "dummy",
                  :timestamp => DateTime.now)
    t.should_not be_valid
  end

  it "has an unique twitter id" do
    t1 = Tweet.create(:text => "text",
                      :twitter_id => "123456",
                      :twitter_user => "foo",
                      :timestamp => DateTime.now)
    t2 = Tweet.create(:text => "another text",
                      :twitter_id => "123456",
                      :twitter_user => "bar",
                      :timestamp => DateTime.now)
    t2.should_not be_valid
  end

  it "should have a timestamp" do
    t = Tweet.new(:text => "text",
                  :twitter_id => "123456",
                  :twitter_user => "dummy")
    t.should_not be_valid
  end
end
