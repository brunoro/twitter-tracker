require 'spec_helper'

describe Tracking do
  before(:each) do
    @user = FactoryGirl.create(:user)
  end
  
  it "should have some keyword" do
    t = Tracking.new(:keywords => "", :user => @user)
    t.should_not be_valid
  end
  
  it "should belong to a user" do
    t = Tracking.new(:keywords => "test")
    t.should_not be_valid
  end
end
